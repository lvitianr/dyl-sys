<%@include file="/taglib.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<title>登录</title>
		<%@include file="common/commonCss.jsp"%>
		<link rel="stylesheet" href="${res}/css/login.css" />
	</head>

	<body class="beg-login-bg">
		<div class="beg-login-box">
			<header>
				<h1>后台登录</h1>
			</header>
			<div class="beg-login-main">
				<form action="login.do" class="layui-form" method="post">
					<div class="layui-form-item">
						<label class="beg-login-icon">
                        <i class="layui-icon">&#xe612;</i>
                    </label>
						<input type="text" name="username" lay-verify="required" <%-- value="${username}" --%> autocomplete="off"  placeholder="这里输入登录名" class="layui-input">
					</div>
					<div class="layui-form-item">
						<label class="beg-login-icon">
                        <i class="layui-icon">&#xe642;</i>
                    </label>
						<input type="password" name="password" lay-verify="required" autocomplete="off"  placeholder="这里输入密码" class="layui-input">
					</div>
					<div class="layui-form-item">
						<!-- <div class="beg-pull-left beg-login-remember">
							<label>记住帐号？</label>
							<input type="checkbox" name="rememberMe" value="true" lay-skin="switch" checked title="记住帐号">
						</div> -->
						<!-- <div class="beg-pull-right">
							<button class="layui-btn layui-btn-primary" lay-submit lay-filter="login">
                            <i class="layui-icon">&#xe650;</i> 登录
                        </button>
						</div> -->
						<div>
							<button style="width: 100%;" class="layui-btn layui-btn-primary" lay-submit lay-filter="login">
                           	 <i class="layui-icon">&#xe650;</i> 登录
                        	</button>
						</div>
						<div class="beg-clear"></div>
					</div>
				</form>
			</div>
			<footer>
				 <p>Beginner © www.zhengjinfan.cn</p>
			</footer>
		</div>
		<%@include file="/WEB-INF/views/common/commonJs.jsp"%>
		<script>
			<c:if test="${not empty loginMsg}"> layer.msg("${loginMsg}",{icon:5});</c:if>
		</script>
	</body>

</html>