<%@include file="/taglib.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>{todo}</title>
		<%@include file="/WEB-INF/views/common/commonCss.jsp"%>
		  <style type="text/css">  
	        .para{  
	            width:10em;/*保证文字不会被半汉字截断,显示10个文字*/  
	            overflow:hidden;/*超出长度的文字隐藏*/  
	            text-overflow:ellipsis;/*文字隐藏以后添加省略号*/  
	            white-space:nowrap;/*强制不换行*/  
	            max-width: 200px;
	        }  
	    </style>  
	</head>
	
	<body>
		<div class="admin-main">
			<!-- 查询条件 -->
			<form  class="layui-elem-quote" action="sysLog!main.do" id="mainForm" method="post">
				<div class="layui-fr">
					<a href="javascript:;" class="layui-btn layui-btn-small" id="search">
						<i class="layui-icon">&#xe615;</i> 查询
					</a>
					<a href="javascript:;" class="layui-btn layui-btn-small" id="rzDownload">
						<i class="layui-icon">&#xe615;</i> 下载系统日志
					</a>
				</div>
    			<div class="layui-input-inline">
			        <label class="layui-form-label">url地址:</label>
			        <div class="layui-input-inline">
			       		  <input type="text" name="url"  value="${sysLog.url}" placeholder="请输入" class="layui-input">
			        </div>
			    </div>
			</form>
			<!-- 主table -->
			<fieldset class="layui-elem-field" >
				<legend>数据列表</legend>
				<div class="layui-field-box layui-form">
					<table class="layui-table admin-table">
						<thead>
							<tr>
								<th class="table-check"><input type="checkbox" name="" lay-skin="primary" lay-filter="allChoose"></th>
								<th>url地址</th>
								<th>参数</th>
								<th>登陆用户</th>
								<th>Ip地址</th>
								<th>访问时间</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${sysLogList}" var="o">
								<tr>
							        <td><input type="checkbox" lay-skin="primary" data-opt="check" data-id="${o.id}"></td>
									<td>${o.url}</td>
									<td class="para" title="${o.para}">${o.para}</td>
									<td>${o.loginuser}</td>
									<td>${o.ip}</td>
									<td><fmt:formatDate value="${o.time}" pattern="yyyy-MM-dd HH:mm" /></td>
							    </tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</fieldset>
			<!-- 分页div -->
			<div class="admin-table-page">
				<div id="page" class="page">
				</div>
			</div>
		</div>
		<!-- 通用js -->
		<%@include file="/WEB-INF/views/common/commonJs.jsp"%>
		<!-- 分页 -->
		<%@include file="/WEB-INF/views/common/page.jsp"%>
		<script>
			//查询方法
			$('#search').click(function(){
				//将分页的 的数据带过来
				$('#mainForm').append($('#pageForm').html());
				showLoading();//显示等待框
				$('#mainForm').submit();
			});
			//日志下载
			$('#rzDownload').click(function(){
				layer.open({
					type: 2,
					title: "下载系统日志",
					content: "log!main.do",
					maxmin: true,
					shade: true,
					area: ['900px', '600px'],
					scrollbar:false,
					shade:0.5,//遮罩透明度
				});
			});
		</script>
	</body>
</html>