<%@include file="/taglib.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>用户管理</title>
		<%@include file="/WEB-INF/views/common/commonCss.jsp"%>
	</head>
	<body>
		<div class="admin-main">
			<!-- 查询条件 -->
			<form  class="layui-elem-quote" action="sysUser!main.do" id="mainForm" method="post">
				<div class="layui-fr">
					<a href="javascript:;" class="layui-btn layui-btn-small" id="search">
						<i class="layui-icon">&#xe615;</i> 查询
					</a>
					<dyl:hasPermission kind="ADD" menuId="${menuId}">
						<a href="javascript:;" class="layui-btn layui-btn-small" id="add">
							<i class="layui-icon">&#xe608;</i> 添加
						</a>
					</dyl:hasPermission>
				</div>
				<div class="layui-input-inline">
			       <label class="layui-form-label">用户名:</label>
			       <div class="layui-input-inline">
			       		  <input type="text" name="username"  value="${sysUser.username}" placeholder="请输入用户名" class="layui-input">
			       </div>
				</div>
			</form>
			<!-- 主table -->
			<fieldset class="layui-elem-field" >
				<legend>数据列表</legend>
				<div class="layui-field-box layui-form">
					<table class="layui-table admin-table">
						<thead>
							<tr>
								<th class="table-check"><input type="checkbox" name="" lay-skin="primary" lay-filter="allChoose"></th>
								<th>用户名</th>
								<th>名称</th>
								<th>邮箱</th>
								<th>电话</th>
								<th>状态</th>
								<th>创建时间</th>
								<th>所属角色</th>
								<th>操作</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${sysUserList}" var="u">
								<tr>
							        <td><input type="checkbox" lay-skin="primary" data-opt="check" data-id="${u.id}"></td>
							        <td>${u.username}</td>
							        <td>${u.name}</td>
							        <td>${u.email}</td>
							        <td>${u.phone}</td>
							        <td>
							        	<c:if test="${u.state eq '0'}"><i class="layui-btn layui-btn-normal layui-btn-mini">正常</i></c:if>
							        	<c:if test="${u.state eq '1'}"><i class="layui-btn layui-btn-danger layui-btn-mini">禁用</i></c:if>
							        </td>
							        <td><fmt:formatDate value="${u.createTime}" pattern="yyyy-MM-dd HH:mm" /></td>
							        <td>${u.roleNames}</td>
							        <td>
							      		 <dyl:hasPermission kind="UPDATE" menuId="${menuId}">
											<a href="javascript:;" class="layui-btn layui-btn-mini" data-id="${u.id}" data-opt="edit">
												<i class="layui-icon">&#xe642;</i> 修改
											</a>
											<a href="javascript:;" class="layui-btn layui-btn-warm layui-btn-mini" data-id="${u.id}" data-opt="rPwd">
												<i class="layui-icon">&#xe631;</i> 重置密码
											</a>
										</dyl:hasPermission>
										<dyl:hasPermission kind="DELETE" menuId="${menuId}">
											<a href="javascript:;" class="layui-btn layui-btn-danger layui-btn-mini" data-id="${u.id}" data-opt="delete">
												<i class="layui-icon">&#xe640;</i> 删除
											</a>
										</dyl:hasPermission> 
									</td>
							    </tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</fieldset>
			<!-- 分页div -->
			<div class="admin-table-page">
				<div id="page" class="page">
				</div>
			</div>
		</div>
		<!-- 通用js -->
		<%@include file="/WEB-INF/views/common/commonJs.jsp"%>
		<!-- 分页 -->
		<%@include file="/WEB-INF/views/common/page.jsp"%>
		<script>
			//查询方法
			$('#search').click(function(){
				//将分页的 的数据带过来
				$('#mainForm').append($('#pageForm').html());
				showLoading();
				$('#mainForm').submit();
			});
			//添加方法
			$('#add').click(function(){
				var para={
					url:"sysUser!sysUserForm.do",
					title:"添加用户",
					btnOK:"保存",
					area:['500px', '600px']
				};
				addOrUpdate(para);
			});
			//修改方法
			$('[data-opt=edit]').click(function(){
				var para={
					url:"sysUser!sysUserForm.do",
					para:"id="+$(this).attr("data-id"),
					title:"修改用户",
					btnOK:"修改",
					area:['500px', '600px']
				};
				addOrUpdate(para);
			});
			//删除方法
			$('[data-opt=delete]').click(function(){
				var userId = $(this).attr("data-id");
				layer.confirm('即将删除该用户及用户下面的所有权限，是否继续?', function(index){
					getJsonDataByPost("sysUser!delete.do","id="+userId,function(data){
						if(data.result){
							layer.alert("删除成功!",function(){
								$('#search').click();
							}); 
						}else{
							l.alert(data.msg);
						}
						layer.close(index);
					},true); 
				});
			});
			//重置密码
			$('[data-opt=rPwd]').click(function(){
				var userId = $(this).attr("data-id");
				layer.confirm('确认重置密码吗?', function(index){
					getJsonDataByPost("sysUser!rPwd.do","id="+userId,function(data){
						if(data.result){
							layer.alert("密码重置成功!"); 
						}else{
							l.alert(data.msg);
						}
						layer.close(index);
					},true); 
				});
			});
		</script>
	</body>
</html>