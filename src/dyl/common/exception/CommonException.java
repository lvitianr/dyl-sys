package dyl.common.exception;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import dyl.common.util.UrlUtil;
/**
 * 公共异常类，记录异常发生的url及异常描述
 * @author Administrator
 *
 */
public class CommonException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	/** 定义打印日志的公共属性 */
	protected Log log = LogFactory.getLog(getClass());
	
	public CommonException(){
		super();
	}
	public CommonException(HttpServletRequest request,Throwable cause) {
		log.error(UrlUtil.getRealUrl(request));//记录发生异常时候的url
		log.error(cause.getMessage(),cause);//记录异常描述
	}
}
