package dyl.sys.action;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexAction extends BaseAction {
	@Resource
	private JdbcTemplate jdbcTemplate;
    @RequestMapping(value = "/index.do")
	public String index() {
		return "/index";
	}
    @RequestMapping(value = "/main.do")
   	public String main(HttpServletRequest request) {
   		return "/main";
   	}
}
