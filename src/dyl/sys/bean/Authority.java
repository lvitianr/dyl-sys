package dyl.sys.bean;


/**
 * 用户权限判断。
 * @author 84829698@qq.com
 *
 */
public class Authority {
	public static String ADD="1";//添加
	public static String DELETE="2";//删除
	public static String EDIT="3";//编辑
	public static String VIEW="4";//查看
	/**
	 * 判断用户是否在基本权限字段上有对应权限。
	 * @param authkind
	 * @return
	 */
	/*public boolean is(Integer menuId, BigDecimal kindId,BigDecimal roleId){
		if(isAdmin()) return true;
		Map<BigDecimal,Set<BigDecimal>> menuKindMap = AuthCache.roleAuthMap.get(roleId);
		if(menuKindMap!=null){
			if (kindId == null){//如果该角色在该菜单下有任何一个权限，则就有查看权限
				if (!menuKindMap.get(menuId).isEmpty()){
					return true;
				}
			}else{
				Set<BigDecimal> kinds = menuKindMap.get(menuId);
				for(BigDecimal k:kinds){
					if(k.equals(kindId))return true;
				}
			}
		}
		return false;
	}*/
/*	public boolean is(BigDecimal menuId,BigDecimal kindId){
		return is(menuId, kindId,null);
	}
	*//**
	 * 判断用户是否在基本权限字段上有权限。
	 * @param authid
	 * @return
	 *//*
	public boolean is(BigDecimal menuId){
		return is(menuId, null);
	}*/

	private boolean isAdmin = false;

	public boolean isAdmin() {
		return isAdmin;
	}

	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}
}
